const Dummy = [
  {
    _id: "6288d52a8fdcc9195b4cd744",
    index: 0,
    username: "GracieHayden",
    email: "graciehayden@qimonk.com",
    experience: 30,
    level: 75,
  },
  {
    _id: "6288d52ab97ca31c51ead8bc",
    index: 1,
    username: "RichmondBerger",
    email: "richmondberger@qimonk.com",
    experience: 47,
    level: 57,
  },
  {
    _id: "6288d52a2b2f1f8c77ab7339",
    index: 2,
    username: "BowersRomero",
    email: "bowersromero@qimonk.com",
    experience: 15,
    level: 53,
  },
  {
    _id: "6288d52af072c1b17b2a3e86",
    index: 3,
    username: "ElsieClark",
    email: "elsieclark@qimonk.com",
    experience: 77,
    level: 32,
  },
  {
    _id: "6288d52ae92b2fa6b04ac7ad",
    index: 4,
    username: "JaimeWeaver",
    email: "jaimeweaver@qimonk.com",
    experience: 65,
    level: 44,
  },
];

export default Dummy;
