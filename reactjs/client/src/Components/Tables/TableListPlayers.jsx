import React, { Component } from "react";

import ButtonPlayers from "../Buttons/ButtonPlayers";
import "./TableListPlayers.css";

class TableListPlayers extends Component {
  render() {
    return (
      <div className="table-container">
        <table className="table">
          <thead>
            <tr>
              <th>Username</th>
              <th>Email</th>
              <th>Experience</th>
              <th>Level</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.props.data.map((tr) => {
              return (
                <tr key={tr._id}>
                  <td>
                    <p>{tr.username}</p>
                  </td>
                  <td>
                    <p>{tr.email}</p>
                  </td>
                  <td>
                    <p>{tr.experience}</p>
                  </td>
                  <td>
                    <p>{tr.level}</p>
                  </td>
                  <td>
                    <ButtonPlayers buttonTitle="Edit Players" onClick={this.props.onCloseEdit} />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default TableListPlayers;
