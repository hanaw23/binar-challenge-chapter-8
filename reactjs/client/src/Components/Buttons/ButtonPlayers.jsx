import React, { Component } from "react";

import "./ButtonPlayers.css";

class ButtonPlayers extends Component {
  render() {
    return (
      <button onClick={this.props.onClick} className="btn" type="button">
        {this.props.buttonTitle}
      </button>
    );
  }
}

export default ButtonPlayers;
