import React, { Component } from "react";

import "./FormPlayers.css";
import Dummy from "../../Data/Dummy";

class Form extends Component {
  state = {
    players: Dummy,
    username: "",
    email: "",
    experience: 0,
    level: 0,
  };

  valid = (item, type) => {
    let itemValue = item.target.value;
    switch (type) {
      case "username": {
        this.setState({ username: itemValue });
      }
      case "email": {
        this.setState({ email: itemValue });
      }
      case "experience": {
        this.setState({ experience: itemValue });
      }
      case "level": {
        this.setState({ level: itemValue });
      }
    }
  };

  handlesubmit = () => {
    let obj = {};
    obj.username = this.state.username;
    obj.email = this.state.email;
    obj.experience = this.state.experience;
    obj.level = this.state.level;

    this.setState({ players: Dummy.push(obj) });
    console.warn("ini data baru: ", this.state.players);
  };

  render() {
    return (
      <div className="input-container">
        <h3 className="title">{this.props.title}</h3>

        <div className="input-form">
          <label className="label-input">Username:</label>
          <span>*</span>
          <input type="text" placeholder="Type your username" className="username-input" onChange={(item) => this.valid(item, "username")}></input>
        </div>
        <div className="input-form">
          <label className="label-input">Email:</label>
          <span>*</span>
          <input type="text" placeholder="Type your email" className="email-input" onChange={(item) => this.valid(item, "email")}></input>
        </div>
        <div className="input-form">
          <label className="label-input">Experience:</label>
          <span>*</span>
          <input type="text" placeholder="Type your experience" className="exp-input" onChange={(item) => this.valid(item, "experience")}></input>
        </div>
        <div className="input-form">
          <label className="label-input">Level:</label>
          <span>*</span>
          <input type="text" placeholder="Type your level" className="lvl-input" onChange={(item) => this.valid(item, "level")}></input>
        </div>
        <div className="button">
          <button type="button" className="close-btn" onClick={this.props.onClose}>
            Close
          </button>
          <button type="submit" className="submit-btn" onClick={this.handlesubmit}>
            Submit
          </button>
        </div>
      </div>
    );
  }
}

export default Form;
