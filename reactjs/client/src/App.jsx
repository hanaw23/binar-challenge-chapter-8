import React, { Component } from "react";

import "./App.css";
import Dummy from "./Data/Dummy";
import FormPlayers from "./Components/Forms/FormPlayers";
import TableListPlayers from "./Components/Tables/TableListPlayers";
import ButtonPlayers from "./Components/Buttons/ButtonPlayers";

class App extends Component {
  state = {
    players: Dummy,
    openAddPlayer: false,
    openSearchPlayer: false,
    openEditPlayer: false,
  };

  handleOpenAddPlayer = () => {
    this.setState({ openAddPlayer: true });
  };

  handleCloseAddPlayer = () => {
    this.setState({ openAddPlayer: false });
  };

  handleOpenSearchPlayer = () => {
    this.setState({ openSearchPlayer: true });
  };

  handleCloseSearchPlayer = () => {
    this.setState({ openSearchPlayer: false });
  };

  handleOpenEditPlayer = () => {
    this.setState({ openEditPlayer: true });
  };

  handleCloseEditPlayer = () => {
    this.setState({ openEditPlayer: false });
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Players Management</h1>
          <div className="container-btn">
            <div className="add-btn">
              <ButtonPlayers buttonTitle="Add Players" onClick={this.handleOpenAddPlayer} />
            </div>
            <div className="src-btn">
              <ButtonPlayers buttonTitle="Search Players" onClick={this.handleOpenSearchPlayer} />
            </div>
          </div>
        </header>

        <main className="main">
          {this.state.openAddPlayer ? (
            <FormPlayers title="Add Players" onClose={this.handleCloseAddPlayer} />
          ) : this.state.openSearchPlayer ? (
            <FormPlayers title="Search Players" onClose={this.handleCloseSearchPlayer} />
          ) : this.state.openEditPlayer ? (
            <FormPlayers title="Edit Players" onClose={this.handleCloseEditPlayer} />
          ) : (
            <TableListPlayers data={this.state.players} onCloseEdit={this.handleOpenEditPlayer} />
          )}
        </main>
      </div>
    );
  }
}

export default App;
